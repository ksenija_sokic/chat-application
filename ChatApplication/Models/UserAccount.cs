﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ChatApplication.Models
{
    public class UserAccount
    {      

        [Key]
        public int UserID { get; set; }
        [Required(ErrorMessage = "Username is required.")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Password is required.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string Email { get; set; }
        
        public virtual List<LoginPeriod> LoginPeriods { get; set; }
        [InverseProperty("SendTo")]
        public virtual List<History> HistoryToUser { get; set; }
        [InverseProperty("SendFrom")]
        public virtual List<History> HistoryFromUser { get; set; }


    }
}