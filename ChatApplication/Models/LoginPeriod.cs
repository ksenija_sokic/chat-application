﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace ChatApplication.Models
{
    public class LoginPeriod
    {
        [Key]
        public int PeriodID { get; set; }
        [DisplayName("Time of login")]
        public DateTime LoginTime { get; set; }
        [DisplayName("Time of logout")]
        public Nullable<DateTime> LogoutTime { get; set; }


        public int UserID { get; set; }
        public virtual UserAccount UserAccount { get; set; }
    }
}