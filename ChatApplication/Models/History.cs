﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ChatApplication.Models
{
    public class History
    {
        [Key]
        public int HistoryID { get; set; }
        public string Message { get; set; }
        public DateTime MessageTime { get; set; }
        
        public virtual UserAccount SendTo { get; set; }
        public virtual UserAccount SendFrom { get; set; }

    }
}