﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ChatApplication.Models
{
    public class ModelDBContext : DbContext
    {
        public ModelDBContext()
            : base("name=ModelDBContext")
        {
        } 

        public DbSet<UserAccount> UserAccounts { get; set; }
        public DbSet<LoginPeriod> LoginPeriods { get; set; }
        public DbSet<History> History { get; set; }
    }
}