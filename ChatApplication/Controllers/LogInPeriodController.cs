﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ChatApplication.Controllers
{
    public class LogInPeriodController : Controller
    {

        HttpClient client = new HttpClient();
        // GET: LogInPeriod
        public async Task<ActionResult> Check(string userName)
        { 
            string[] url = Request.Url.AbsoluteUri.Split(new string[] { "LogInPeriod" }, StringSplitOptions.None);
            string urlPath = url[0] + "api/Get";


            HttpResponseMessage responseMessage = await client.GetAsync(urlPath + "/?username=" + userName);            
            var responseData = responseMessage.Content.ReadAsStringAsync().Result;
            bool exists = JsonConvert.DeserializeObject<bool>(responseData);

            if (exists)
            {
                return RedirectToAction("Welcome", "UserAccounts", new { username = userName, message = userName+ " is online" });
            }
            else {
                return RedirectToAction("Welcome","UserAccounts",new { username=userName,message= userName+ " is not currently online" });
            }
            
        }

    
    }
}
