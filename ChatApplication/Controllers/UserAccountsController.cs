﻿using ChatApplication.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ChatApplication.Controllers
{
    public class UserAccountsController : Controller
    {        
         HttpClient client = new HttpClient();
        
        public ActionResult Index(string username)

        {
            ViewBag.UserName = username;
            return View();
        }

        public async Task<ActionResult> Welcome(string username, string message)
        {
            string[] url = Request.Url.AbsoluteUri.Split(new string[] { "UserAccounts" }, StringSplitOptions.None);
            string urlPath = url[0] + "api/GetAll";

            HttpResponseMessage responseMessageGet = await client.GetAsync(urlPath);
            var responseData = responseMessageGet.Content.ReadAsStringAsync().Result;
            List<UserAccount> accounts = JsonConvert.DeserializeObject<List<UserAccount>>(responseData);
            ViewBag.UserName = username;
            ViewBag.UserStatus = message;
            
            return View(accounts);
        }

        public ActionResult Register(string message)
        {
            ViewBag.AccountExists = message;
            return View();
        }        

        // POST: Create/Create
        [HttpPost]
        public async Task<ActionResult> Register(UserAccount userAccount)
        {

            try
            {
                string[] url = Request.Url.AbsoluteUri.Split(new string[] { "UserAccounts" }, StringSplitOptions.None);
                string urlPath = url[0] + "api/UserAccountsApi" + url[1];

                UserAccount user = new UserAccount();
                user.Username = userAccount.Username;
                user.Password = userAccount.Password;
                user.Email = userAccount.Email;

                HttpResponseMessage responseMessageGet = await client.GetAsync(urlPath + "/" + "?username=" + userAccount.Username + "&password=" + userAccount.Password);
                var responseData = responseMessageGet.Content.ReadAsStringAsync().Result;

                if (responseData == String.Empty)
                {
                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync(urlPath, user);                   
                }
                else
                {
                    return RedirectToAction("Register", new { message = "Username or Password already exists" });
                }

                return RedirectToAction("LogIn");
            }
            catch
            {
                return View();
            }
        }


        public ActionResult LogIn(string message)
        {
            ViewBag.InvalidAccount = message;
            return View();
        }

        // POST: Create/Create
        [HttpPost]
        public async Task<ActionResult> LogIn(UserAccount userAccount)
        {
           
            try
            {
                string[] url = Request.Url.AbsoluteUri.Split(new string[] { "UserAccounts" }, StringSplitOptions.None);

                UserAccount user = new UserAccount();
                user.Username = userAccount.Username;
                user.Password = userAccount.Password;

                HttpResponseMessage responseMessageGet = await client.GetAsync(url[0] + "api/Login" + "/" + "?username=" + userAccount.Username + "&password=" + userAccount.Password);
                var responseData = responseMessageGet.Content.ReadAsStringAsync().Result;
                UserAccount myAccount = JsonConvert.DeserializeObject<UserAccount>(responseData);
                
                if (myAccount.UserID == 0)
                    return RedirectToAction("Register", new { message = "You don't have an account, please register" });
                else
                {
                    LoginPeriod logInPeriod = new LoginPeriod();
                    logInPeriod.LoginTime = DateTime.Now;
                    logInPeriod.UserID = myAccount.UserID;
                    Session["UserId"] = myAccount.UserID;

                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync(url[0] + "api/PostLoginTime", logInPeriod);

                    return RedirectToAction("Welcome", new { username = myAccount.Username });
                }
                
            }
            catch
            {
                return RedirectToAction("LogIn", new { message = "Invalid account" });
            }
        }

        public async Task<ActionResult> LogOut()
        {
            string[] url = Request.Url.AbsoluteUri.Split(new string[] { "UserAccounts" }, StringSplitOptions.None);
            string urlPath = url[0] + "api/LogInPeriodApi";
            

            HttpResponseMessage responseMessage = await client.GetAsync(urlPath + "/" + Session["UserId"]);

            HttpResponseMessage responseMessageUser = await client.GetAsync(url[0] + "api/UserAccountsApi" + "/" + Session["UserId"]);
            var responseData = responseMessageUser.Content.ReadAsStringAsync().Result;
            UserAccount myAccount = JsonConvert.DeserializeObject<UserAccount>(responseData);

            return RedirectToAction("Index", "UserAccounts", myAccount.Username);
        }

    }
}