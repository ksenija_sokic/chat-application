﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ChatApplication.Models;

namespace ChatApplication.Controllers
{
    public class UserAccountsApiController : ApiController
    {
        private ModelDBContext db = new ModelDBContext();

        [Route("api/GetAll")]
        public List<UserAccount> GetAll()
        {
            List<LoginPeriod> accountIds = db.LoginPeriods.Where(period => period.LogoutTime == null).ToList();

            List<UserAccount> accounts = new List<UserAccount>();

            foreach (var account in accountIds)
            {
                UserAccount user = db.UserAccounts.Find(account.UserID);
                accounts.Add(user);
            }

            return accounts;
        }

        // GET: api/UserAccountsApi
        [Route("api/Login")]
     
        public UserAccount GetUserAccounts(string username, string password)
        {
            UserAccount userAccount = db.UserAccounts.Where(user => (user.Username == username) && (user.Password == password)).FirstOrDefault();
            return userAccount;
        }

        public UserAccount Get(int id)
        {
            UserAccount userAccount = db.UserAccounts.Where(user => user.UserID == id).FirstOrDefault();
            return userAccount;
        }

        // GET: api/UserAccountsApi/5
        [ResponseType(typeof(UserAccount))]
        public IHttpActionResult GetUserAccount(string username,string password)
        {
            UserAccount userAccount = db.UserAccounts.Where(user => (user.Username == username) || (user.Password == password)).FirstOrDefault();
            if (userAccount == null)
                return NotFound();
            else
                return Ok(userAccount);
        }
       

     
        // POST: api/UserAccountsApi
        [ResponseType(typeof(UserAccount))]
        public IHttpActionResult PostUserAccount(UserAccount userAccount)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            db.UserAccounts.Add(userAccount);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = userAccount.UserID }, userAccount);
        }

        // DELETE: api/UserAccountsApi/5
        [ResponseType(typeof(UserAccount))]
        public IHttpActionResult DeleteUserAccount(int id)
        {
            UserAccount userAccount = db.UserAccounts.Find(id);
            if (userAccount == null)
            {
                return NotFound();
            }

            db.UserAccounts.Remove(userAccount);
            db.SaveChanges();

            return Ok(userAccount);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserAccountExists(int id)
        {
            return db.UserAccounts.Count(e => e.UserID == id) > 0;
        }
    }
}