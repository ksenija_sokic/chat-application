﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ChatApplication.Models;

namespace ChatApplication.Controllers
{
    public class LogInPeriodApiController : ApiController
    {
        private ModelDBContext db = new ModelDBContext();

        // GET: api/LogInPeriodApi
        public IQueryable<LoginPeriod> GetLoginPeriods()
        {
            return db.LoginPeriods;
        }

        // GET: api/LogInPeriodApi/5
        [ResponseType(typeof(LoginPeriod))]
        public IHttpActionResult GetLoginPeriod(int id)
        {
            LoginPeriod period = db.LoginPeriods.Where(p => (p.UserID == id && p.LogoutTime==null)).OrderByDescending(o => o.UserID).FirstOrDefault();
            period.LogoutTime = DateTime.Now;
            
            db.SaveChanges();
            
            if (period == null)
            {
                return NotFound();
            }

            return Ok(period);
        }


        [Route("api/Get")]
        public bool Get(string username)
        {
            try
            {
                var Id = db.UserAccounts.Where(u => u.Username == username).FirstOrDefault().UserID;
                if (db.LoginPeriods.Any(user => user.UserID == Id))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
            return false;
        }



        // PUT: api/LogInPeriodApi/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLoginPeriod(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            LoginPeriod period = db.LoginPeriods.Last(p=>p.UserID== id);
            period.LogoutTime = DateTime.Now;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LoginPeriodExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/LogInPeriodApi
        [ResponseType(typeof(LoginPeriod))]
        [Route("api/PostLoginTime")]        
        public IHttpActionResult PostLoginPeriod(LoginPeriod loginPeriod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.LoginPeriods.Add(loginPeriod);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = loginPeriod.PeriodID }, loginPeriod);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LoginPeriodExists(int id)
        {
            return db.LoginPeriods.Count(e => e.PeriodID == id) > 0;
        }
    }
}