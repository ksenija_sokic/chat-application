﻿$(function () {
    // Reference the auto-generated proxy for the hub.
    var chat = $.connection.chatHub;
    // Create a function that the hub can call back to display messages.

    chat.client.addNewUsersToPage = function (name) {

        // Add the message to the page.
        $('#discussion').append('<li><strong>' + htmlEncode(name)
            + '</li>');
    };
    
  
    $.connection.hub.start().done(function () {
        $('#sendmessage').click(function () {
            // Call the Send method on the hub.
            chat.server.send($('#displayname').val());
        });
    });
            });
// This optional function html-encodes messages for display in the page.
function htmlEncode(value) {
    var encodedValue = $('<div />').text(value).html();
    return encodedValue;
}
