﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Web.Http;
using Microsoft.AspNet.SignalR.Hubs;

namespace ChatApplication.SignalR_Chat.Hubs
{
    public abstract class SignalRBase<THub> : ApiController where THub:IHub
    {
        private readonly Lazy<IHubContext> _hub = new Lazy<IHubContext>(
            ()=>GlobalHost.ConnectionManager.GetHubContext<THub>()
            );

        protected IHubContext Hub
        {
            get { return _hub.Value; }
        }
    }
}