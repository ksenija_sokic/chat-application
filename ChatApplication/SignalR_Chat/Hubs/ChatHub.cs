﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Net.Http;
using Microsoft.AspNet.SignalR.Hubs;
using ChatApplication.Models;

namespace ChatApplication.SignalR_Chat.Hubs
{
    public class ChatHub : Hub
    {
        public void Send(string name)
        {
            Clients.All.addNewUsersToPage(name);
        }
        public void Send(string name, string message)
        {
            Clients.All.addNewMessageToPage(name, message);
        }
        public void Remove(string name)
        {
            Clients.All.removeUserFromPage(name);
        }
        
    }
}